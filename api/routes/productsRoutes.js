import express from 'express';
const router =  express.Router();
import asyncHandler from 'express-async-handler';
import Product from '../models/productsModel.js';

// Desc         Fetch all products 
// GET          api/products/
// Access       Public 
router.get('/', asyncHandler(async (req, res) => {
    const products = await Product.find({});
    res.json(products);
}))

// Desc         Fetch single product   
// GET          api/products/:id
// Access       Public 
router.get('/:id', asyncHandler(async(req, res) => {
    const product = await Product.findById(req.params.id)
    if(product){
        res.json(product)
    }else{
        res.status(404)
        throw new Error('Product not found');

    }
    res.json(product);
}))



export default router;
