import express  from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import connectDB from './config/db.js'
import colors from 'colors';
import productsRoutes from './routes/productsRoutes.js';
import { notFound, errorHandler } from './middleware/errorMiddleware.js';


dotenv.config();

// express
const app = express();
const router = express.Router();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// cors
app.use(cors()); 
// MongoDB connection
connectDB();
// error middleware



// routes
app.get('/', (req, res) => {
    res.send('API is running');
})
app.use('/api/products', productsRoutes)
app.use(notFound);
app.use(errorHandler);
PORT = process.env.PORT || 5000
app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV}, on port ${PORT}`.cyan.underline));


const PORT = process.env.PORT |