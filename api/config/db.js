import mongoose from 'mongoose';

const connectDB = async () => {
    try {
        const con = await mongoose.connect(process.env.DB_CONNECTION, {
            useUnifiedTopology:true,
            useNewUrlParser: true,
            useCreateIndex: true,
        })
        console.log(`mongoDB connected: ${con.connection.host}`.cyan.underline)
    } catch (error) {
        console.error(`Èrror: ${error.message}`)
        process.exit(1)
    }
}
export default connectDB;