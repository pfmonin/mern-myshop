import {
    PRODUCT_LIST_REQUEST, 
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,

    PRODUCT_DETAILS_REQUEST, 
    PRODUCT_DETAILS_SUCCESS,
    PRODUCT_DETAILS_FAIL
} from '../constants/productConstant'

// Reducer take 2 param : 1. The initial state (usually null, empty...) and 2. the action => pattern
export const productListReducers = ( state={ products: [] }, action ) => {

    // Initie les 3 types d'actions : Le "fetch request", if success et if failed  
    switch(action.type){
        case PRODUCT_LIST_REQUEST:
            return { loading: true, products: [] } 
        case PRODUCT_LIST_SUCCESS: 
            return { loading: false, products: action.payload } 
        case PRODUCT_LIST_FAIL: 
            return { loading: false, error: action.payload }
        
        // par default en else on retourne le state
        default:
            return state; 
    }
}

export const productDetailsReducers = ( state={ product: { reviews: [] } }, action ) => {

    // Initie les 3 types d'actions : Le "fetch request", if success et if failed  
    switch(action.type){
        case PRODUCT_DETAILS_REQUEST:
            return { loading: true, ...state } 
        case PRODUCT_DETAILS_SUCCESS: 
            return { loading: false, product: action.payload } 
        case PRODUCT_DETAILS_FAIL: 
            return { loading: false, error: action.payload }
        
        // par default en else on retourne le state
        default:
            return state; 
    }
}