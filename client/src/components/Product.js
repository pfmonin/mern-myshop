import React from 'react';
import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import Rating from '../components/Rating';
import PropTypes from 'prop-types';

const Product = ({ product }) => {
    return (
       
        <Card className="my-3 p-3 rounded">
            <Link to={`/product/${product._id}`}>
                <Card.Img src={product.image} variant='top' />
            </Link>
            <Card.Body>
                <Link to={`/product/${product._id}`}>
                    <Card.Title as='div'> <strong>{product.name}</strong> </Card.Title>                    
                </Link>
            <Card.Text as="div"> 
                <div className="my-3">
                   <Rating 
                        value={product.rating} 
                        text={`${product.numReviews} reviews`}
                        // color='gold' 
                        />
                </div>
            </Card.Text>
    <Card.Text as="h3">${product.price}</Card.Text>

            </Card.Body>
        </Card>
      
    )
}

// alternative to color = gold in props => set it as a default props
Rating.defaultProps = {
    color: "gold",
}
// Sert a type-inté les valeurs donnée pr les props (non obligatoire, bonne pratique)
Rating.propTypes = {
    value: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
}

export default Product
