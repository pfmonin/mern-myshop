import React from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap';
import { LinkContainer} from 'react-router-bootstrap';

const Header = () => {
    return (
        <header>

            <Navbar bg="dark" variant='dark' expand="lg" collapseOnSelect style={{padding:"5px 15px "}}>
                <LinkContainer to='/' >
                    <Navbar.Brand>
                        <img src="https://cdn.pixabay.com/photo/2016/12/28/08/15/hummingbird-1935665_960_720.png" style={{width: '50px', height:'50px', borderRadius:'50%'}} alt=""/>
                        My Shop
                    </Navbar.Brand>
                </LinkContainer>
                
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <LinkContainer to="/cart">
                            <Nav.Link> <i className="fas fa-shopping-cart"></i> Cart</Nav.Link>                        
                        </LinkContainer>
                        <LinkContainer to="/login">
                            <Nav.Link> <i className="fas fa-user"></i>Sign In </Nav.Link>                    
                        </LinkContainer>
                    </Nav>                    
                </Navbar.Collapse>            
            </Navbar>
            
        </header>
    )
}

export default Header
